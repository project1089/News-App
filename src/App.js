import React from "react";

import Header from "./Components/Header/Header";
import ReadNow from "./Components/Readnow/Readnow";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Footer from "./Components/Footer/Footer";
import Everything from "./Components/Everything/Everything";
import Country from "./Components/Country/Country";
import Category from "./Components/Category/Category";
import Source from "./Components/Source/Source";

class App extends React.Component {
  render() {
    return (
      // please add your code
      <div>
        <Router>
          <Header />
          <Switch>
            <Route exact path="/" component={ReadNow} />

            <Route exact path="/everything" component={Everything} />

            <Route exact path="/country" component={Country} />

            <Route exact path="/category" component={Category} />

            <Route exact path="/source" component={Source} />
          </Switch>

          <Footer />
        </Router>
      </div>
    );
  }
}
export default App;
