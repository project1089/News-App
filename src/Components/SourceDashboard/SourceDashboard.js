import React from 'react'
import './SourceDashboard.css'

export default function sourceDashboard(props) {
   
    //Printing the source cards
    return (
            <div className="container mt-2 col-md-4 p-4">
                <div className="row">

                        <div className="card mt-5 border border-primary card-height">
                            <div className="card-body">

                                <h5 className = "text-dark">{props.description.substring(0,40)}</h5>
                                <h5 className = "text-dark">URL: {props.url}</h5>
                                <h5 className = "text-success">Source: {props.name}</h5>

                                <button className="btn btn-primary">Read Later</button>
                            </div>
                        </div>
                </div>
            </div>

    )
}
