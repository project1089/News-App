import React, { useEffect } from 'react'
import { useState } from 'react'
import SourceDashboard from '../SourceDashboard/SourceDashboard'
import './Source.css';


export default function Source() {
    const [news, setnews] = useState([])
    const apikey = "50a5fdd1807742b5920797409440004d"

    //Fetching the sources
    useEffect(() => {
        fetch(`https://newsapi.org/v2/top-headlines/sources?apiKey=${apikey}`)
            .then(response => response.json())
            .then(data => {
                setnews(data.sources)
            })

    })
    
    return (
        <div>
            <div className="container">
                <div className="row mt-3">
                        {
                            news.map(item => <SourceDashboard   key={item.id} name={item.name} description={item.description} url = {item.url}
                                country = {item.country} />)

                        }
                </div>
            </div>
        </div>

    )
}

