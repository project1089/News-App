import React, { useEffect } from 'react'
import { useState } from 'react'
import Dashboard from '../Dashboard/Dashboard'
import './ReadNow.css';


export default function Readnow() {
    const [news, setnews] = useState([])
    const apikey = "50a5fdd1807742b5920797409440004d"

    useEffect(() => {
        fetch(`https://newsapi.org/v2/top-headlines?country=in&apikey=${apikey}`)
            .then(response => response.json())
            .then(data => {
                //console.log(data)
                setnews(data.articles)
            })

    })
    const addNewsFunction = (newss) => {
        fetch("http://localhost:3004/news",{
            method : "POST",
            headers : {
                "Content-type" : "application/json"
            },
            body : JSON.stringify(newss)
        })
        setnews([...news,newss])
    }
    return (
        <div>
            <div className="container">
                <div className="row mt-5">
                        {
                            news.map(item => <Dashboard contactEvent={addNewsFunction} key = {item.url} titles={item.title} authors={item.author} images={item.urlToImage} description = {item.description}
                                urlToDescription = {item.url} />) 

                        }
                </div>
            </div>
        </div>

    )
}

