import React, { useEffect, useState } from 'react'
import Dashboard from '../Dashboard/Dashboard'

export default function Category() {
    const [category, setcategory] = useState([])
    const [search, setsearch] = useState("")
    const apikey = "5aa771ce53314bc7bf375b2edf6a8002"
    //Fetching the news according to the categories like sports,entertainment
    useEffect(() =>{
        const fetchapi = async() => {
            const url = `https://newsapi.org/v2/top-headlines?country=in&category=${search}&apiKey=${apikey}`
            const response = await fetch(url)
            const resJson = await response.json()
            setcategory(resJson.articles)
        }
        fetchapi()
    }
    )
    const addNewsFunction = (newss) => {
        fetch("http://localhost:3004/news",{
            method : "POST",
            headers : {
                "Content-type" : "application/json"
            },
            body : JSON.stringify(newss)
        })
        setcategory([...category,newss])
    }
    return (
        <div className="container mt-5">
            <div className="row mt-5">
                <div className = "col-md-4 offset-md-4">
                <input className="form-control me-2 mt-5 border border-primary"  
                onChange={(event) => {setsearch(event.target.value)}} defaultValue = "in" type="search" placeholder="Enter the category(e.g. sports)" aria-label="Search" />
                 <div className = "mb-2">
                        <button className = "btn btn-primary col-12 mt-2">Search</button>
                    </div>
            </div>
            </div>
            {!category ? (
                <p>No data found</p>

            ):(
                <div className="container">
                <div className="row">
                        {
                            category.map(item => <Dashboard contactEvent={addNewsFunction} key = {item.url} titles={item.title} authors={item.author} images={item.urlToImage} description = {item.description}
                                urlToDescription = {item.url} />)

                        }
                    
                </div>
            </div>
            )}
            </div>
            
    )
}
