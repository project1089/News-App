import React, { useEffect, useState } from 'react'
import Dashboard from '../Dashboard/Dashboard'
import './Country.css';

export default function Country() {
    //Fetching the data according to country codes
    const [country, setcountry] = useState([])
    const [search, setsearch] = useState("")
    const apikey = "50a5fdd1807742b5920797409440004d"
    useEffect(() =>{
        const fetchapi = async() => {
            const url = `https://newsapi.org/v2/top-headlines?country=${search}&apikey=${apikey}`
            const response = await fetch(url)
            const resJson = await response.json()
            setcountry(resJson.articles)
        }
        fetchapi()
    }
    )
    const addNewsFunction = (newss) => {
        fetch("http://localhost:3004/news",{
            method : "POST",
            headers : {
                "Content-type" : "application/json"
            },
            body : JSON.stringify(newss)
        })
        setcountry([...country,newss])
    }
    return (
        <div className="container mt-5">
            <div className="row mt-5">
                <div className = "col-md-4 offset-md-4">
                <input id="countrycode" className="form-control me-2 mt-5 border border-primary"  
                onChange={(event) => {setsearch(event.target.value)}} type="search" placeholder="Enter the country code(e.g. in,us)" aria-label="Search" />
                <div className = "mb-2">
                        <button className = "btn btn-primary col-12 mt-2">Search</button>
                    </div>
            </div>
            </div>
            {!country ? (
                <p>No data found</p>

            ):(
                <div className="container ">
                <div className="row">
                        {
                            country.map(item => <Dashboard contactEvent={addNewsFunction} key = {item.url} titles={item.title} authors={item.author} images={item.urlToImage} description = {item.description}
                                urlToDescription = {item.url} />)

                        }
                     
                </div>
            </div>
            )}
            </div>
            
    )
}
