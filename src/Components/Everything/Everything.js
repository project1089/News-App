import React, { useEffect } from 'react'
import { useState } from 'react'
import Dashboard from '../Dashboard/Dashboard'

export default function Everything() {
    const [news, setnews] = useState([])
    const apikey = "e10c28296661466e8b85e4bd09a81b30"
    //Fetching the news of all categories
    useEffect(() => {
        fetch(`https://newsapi.org/v2/everything?q=bitcoin&apiKey=${apikey}`)
            .then(response => response.json())
            .then(data => {
                setnews(data.articles)
            })

    })
    const addNewsFunction = (newss) => {
        console.log(newss);
        fetch("http://localhost:3004/news",{
            method : "POST",
            headers : {
                "Content-type" : "application/json"
            },
            body : JSON.stringify(newss)
        })
        setnews([...news,newss])
    }
    return (
        <div>
            <div className="container">
                <div className="row">
                    <div>
                        {
                            news.map(item => <Dashboard contactEvent={addNewsFunction} key = {item.url} titles={item.title} authors={item.author} images={item.urlToImage} description = {item.description}
                                urlToDescription = {item.url} />)

                        }
                    </div >
                </div>
            </div>
        </div>

    )
}

