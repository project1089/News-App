import React from 'react'
import './Dashboard.css'

export default function Dashboard(props) {
    const addnews=()=>{
        // console.log(title,author,image);
        props.contactEvent({description:props.description,url:props.urlToDescription})
    }
    //Printing the data fetched in forms of cards
    return (
            <div className="container col-md-4 p-4"  >     
                <div className="row">

                        <div className="card mt-3 border border-primary card-style">
                            <img src={props.images} className="card-img-top mt-2" alt="..." />
                            <div className="card-body">
                                <h5 className="card-title text-dark" >{props.titles.substring(0,30)}</h5>
                                <button className="btn text-light btn-style" onClick={addnews}>Read Later</button>
                            </div>
                        </div>
                    
                </div>
            </div>
          
    )
}
