import React from "react";
import { Link } from "react-router-dom";
import "./Header.css";

export default function Header() {
  return (
    <nav>
      <div class="logo">
      <i class="fab fa-slack"></i>
        News Manager
      </div>
      <input type="checkbox" id="click" />
      <label for="click" class="menu-btn">
        <i class="fas fa-bars"></i>
      </label>
      <ul>
        <Link to="/" className="nav-link text-light active">
          Home
        </Link>
        <Link to="/country" className="nav-link text-light">
          Countries
        </Link>
        <Link to="/category" className="nav-link text-light">
          Categories
        </Link>
        <Link to="/source" className="nav-link text-light">
          Sources
        </Link>
      </ul>
    </nav>
  );
}

