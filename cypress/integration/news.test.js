context("News Manager e2e testing",()=>{
    beforeEach(() => {
        cy.visit("/")
    })

    it("Should check for login",() =>{
        cy.get("#username").type("admin")
        cy.get("#password").type("password")
        cy.get("#btnLogin").click()
        cy.wait(1000)
        cy.get("#header").should("have.text","Trending News")

    })
    it("Should show the country news by entering country's id",() =>{
        cy.get("#username").type("admin")
        cy.get("#password").type("password")
        cy.get("#btnLogin").click()
        cy.visit("/country")
        cy.get("#countrycode").type("in")
        cy.get("#heading").should("have.text","Trending News")
    })
    it("Should show the sources of the news",() =>{
        cy.get("#username").type("admin")
        cy.get("#password").type("password")
        cy.get("#btnLogin").click()
        cy.visit("//source")
        cy.get("#heading2").should("have.text","Sources")
    })
})